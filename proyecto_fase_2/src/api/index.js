const express = require('express');
const app = express();
const port = 3001;

app.use(express.json());

app.post('/api/send-message', (req, res) => {
  const message = req.body.message;

  // Aquí puedes realizar cualquier procesamiento o lógica adicional con el mensaje

  // Simulación de respuesta del cliente dos
  const responseMessage = `Respuesta recibida: ${message}`;

  res.json({ message: responseMessage });
});

app.listen(port, () => {
  console.log(`API backend escuchando en http://localhost:${port}`);
});
