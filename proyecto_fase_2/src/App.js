import React, { useState, useEffect } from 'react';
import { Box, TextField, Button, Paper, Typography } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import MicIcon from '@mui/icons-material/Mic';
import SendIcon from '@mui/icons-material/Send';
import { saveAs } from 'file-saver';

import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition';


const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: '100vh',
  },
  paper1: {
    width: '80%',
    maxWidth: '800px',
    margin: '20px auto',
  },
  paper2: {
    width: '80%',
    maxWidth: '800px',
    flexGrow: 1,
    margin: '0 auto',
  },
};
const App = () => {
  const [inputText, setInputText] = useState('');
  const [chatHistory, setChatHistory] = useState([]);
  const [isListening, setIsListening] = useState(false);
  const { transcript, resetTranscript } = useSpeechRecognition();

  useEffect(() => {
    if (!isListening && transcript) {
      setInputText(transcript);
    }
  }, [isListening, transcript]);

  const handleInputChange = (event) => {
    setInputText(event.target.value);
  };

  const handleSpeechRecognition = () => {
    if (isListening) {
      SpeechRecognition.stopListening();
      setIsListening(false);
    } else {
      SpeechRecognition.startListening({ continuous: true, lang: 'es-ES' });
      setIsListening(true);
    }
  };

  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      handleSubmit();
    }
  };
  const handleSubmit = async () => {

    try
    {
      const textToSubmit = (inputText || transcript).trim();
      
      if (textToSubmit === '') {
        return; 
      }

      const response = await fetch(
        `https://localhost:7118/api/Test/${encodeURIComponent(textToSubmit)}`
      );

      if(!response.ok)
        {
          return;
        }
      
const data = await response.json();
let dataMessage = "";

if (data.resultResopnse.type === 3) {
  const base64Data = data.resultResopnse.resultContent;
  const byteCharacters = atob(base64Data);
  const byteNumbers = new Array(byteCharacters.length);

  for (let i = 0; i < byteCharacters.length; i++) {
    byteNumbers[i] = byteCharacters.charCodeAt(i);
  }

  const byteArray = new Uint8Array(byteNumbers);
  const blob = new Blob([byteArray], { type: 'application/pdf' });

  saveAs(blob, 'file.pdf');
  // const blob = new Blob([data.resultResopnse.resultContent], { type: 'application/pdf' });
  // saveAs(blob, "file.pdf");
  dataMessage = data.resultResopnse.headerText;
} else {
  dataMessage = data.resultResopnse.resultContent;
}

      const newMessage = {
        message: `Cliente: ${textToSubmit}`,
        isUser: true
      };
      const newResponse = {
        message: `Bot: ${dataMessage}`,
        isUser: false
      };

      if ('speechSynthesis' in window) {
        const speech = new SpeechSynthesisUtterance(dataMessage);
        window.speechSynthesis.speak(speech);
      } else {
        console.log('La síntesis de voz no es compatible en este navegador.');
      }

      setChatHistory([...chatHistory, newMessage, newResponse]);
      setInputText('');
      resetTranscript();
    }catch (error) {
      console.error('Ocurrió un error en el llamado:', error);
      // Aquí puedes agregar el manejo del error, como mostrar un mensaje al usuario
    }
  };

  return (
    <div style={styles.container}>
      <Paper elevation={3} style={styles.paper1}>
          <TextField
            label="Ingrese el texto o hable"
            value={isListening ? transcript : inputText}
            onChange={handleInputChange}
            onKeyPress={handleKeyPress}
            fullWidth
            margin="normal"
          />
          <Box display="flex" justifyContent="start">
      <IconButton
        color={isListening ? 'secondary' : 'primary'}
        onClick={handleSpeechRecognition}
        aria-label={isListening ? 'Detener' : 'Hablar'}
      >
        <MicIcon />
      </IconButton>
      <IconButton color="primary" onClick={handleSubmit} aria-label="Enviar">
        <SendIcon />
      </IconButton>
          </Box>






        </Paper>
      <Paper elevation={1} style={styles.paper2}>
           {chatHistory.map((message, index) => (


          <Typography
            key={index}
            variant="body1"
            
            style={{ marginBottom: '8px', color: message.isUser ? 'blue' : 'green' }}
          >
              <Box component="div" dangerouslySetInnerHTML={{ __html: message.message }} />
          </Typography>
        ))}
        </Paper>
        </div>
  );
};

export default App;
