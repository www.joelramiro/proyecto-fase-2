﻿using ProyectoFase2.Shared.Models.Manager.Interpretes.HttpClients;

namespace ProyectoFase2.WitInterprete.HttpClients
{
    public class HttpClientWIT : IHttpClientWIT
    {
        private readonly HttpClient httpClient;

        public HttpClientWIT(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public HttpClient HttpClient()
        {
            return this.httpClient;
        }
    }
}
