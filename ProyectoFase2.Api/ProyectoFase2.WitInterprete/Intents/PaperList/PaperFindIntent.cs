﻿using Microsoft.EntityFrameworkCore;
using ProyectoFase2.Database.Context;
using ProyectoFase2.Database.Models;
using ProyectoFase2.Shared.Models.Manager;
using ProyectoFase2.Shared.Models.Manager.Interpretes;
using ProyectoFase2.WitInterprete.Intents;
using ProyectoFase2.WitInterprete.Interpretes.WIT.Models.Response;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace ProyectoFase2.WitInterprete.Intents.Paper
{
    public class PaperResumeFindIntent : IIntentResult
    {
        private readonly IRepository<PaperResume> paperResumeRepository;
        private readonly IRepository<KeywordPaper> keywordPaperRepository;

        public PaperResumeFindIntent(
            IRepository<PaperResume> paperResumeRepository,
            IRepository<KeywordPaper> keywordPaperRepository
            )
        {
            this.paperResumeRepository = paperResumeRepository;
            this.keywordPaperRepository = keywordPaperRepository;
        }

        public string IntentName => "get_paper_resumes";

        public async Task<ServiceResult<InterpreteContentResponse>> Response(string content)
        {
            try
            {
                var myJsonObjectResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<PaperFindDesiarizable>(content);
                if (myJsonObjectResponse == null)
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "Internal server error." });
                }
                if (myJsonObjectResponse.entities.papers == null || !myJsonObjectResponse.entities.papers.Any())
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se encontraron elementos." });
                }

                var words = myJsonObjectResponse.entities?.objects?.Select(o => o.value);
                if (words == null)
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se pudo interpretar el mensaje." });
                }

                var list = new List<KeywordPaper>();

                foreach (var word in words)
                {
                    var found = await this.keywordPaperRepository.All()
                        .Include(d => d.PaperResume)
                        .Where(i =>
                            i.Name.Contains(word) ||
                            InventoryDatabaseContext.SoundEx(i.Name) == InventoryDatabaseContext.SoundEx(word)
                        )
                        .ToListAsync();

                    //var regexMatches = found.Where(i => Regex.IsMatch(i.Name, word, RegexOptions.IgnoreCase)).ToList();

                    list.AddRange(found);
                    //list.AddRange(regexMatches);
                }



                var PaperResume = list.GroupBy(l => l.PaperResume);


                var send = new List<string>();
                if (PaperResume == null || !PaperResume.Any())
                {

                    send = (new List<string>() { "Hola. <ul> No se encontraron estados del arte que hablen de los temas mencionados </ul><ul> <b>No te recomiendo ese tema.</b></ul> " }).ToList();
                }
                else
                {
                    send = PaperResume.Select(i => $"<li>Número:{i.Key.Id}. Nombre:{i.Key.Name}. Llave(s): {string.Join(",", i.Select(ii => ii.Name))}</li>").ToList();
                    send = (new List<string>() { "Hola. <ul> " }).Union(send).Union(new List<string>() { " </ul> " }).ToList();

                    if (PaperResume.Count() >=3)
                    {
                        send.Add("<ul> <b>Es posible, pero es un tema muy tratado.</b></ul>");
                    }
                    else
                    {
                        send.Add("<ul> <b>Es un buen tema a desarrollar.</b></ul>");
                    }
                }

                var stringJoin = string.Join("", send.ToArray());

                return ServiceResult<InterpreteContentResponse>.SuccessResult(new InterpreteContentResponse
                {
                    HeaderText = string.Empty,
                    Type = InterpreteResponseType.Html,
                    ResultContent = stringJoin,
                });
            }
            catch (Exception e)
            {
                return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "Internal server error." });
            }
        }
    }

    public class KeyValue
    {
        public string Key { get; set; }
        public PaperResume Value { get; set; }
    }
}
