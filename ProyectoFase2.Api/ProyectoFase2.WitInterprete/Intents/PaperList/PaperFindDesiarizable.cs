﻿using Newtonsoft.Json;
using ProyectoFase2.WitInterprete.Intents.ItemPosition;
using ProyectoFase2.WitInterprete.Interpretes.WIT.Models.Response;

namespace ProyectoFase2.WitInterprete.Intents.Paper
{
    public class PaperFindDesiarizable : Root
    {
        [JsonProperty("entities")]
        public Entities entities { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Entities
    {
        [JsonProperty("jcibot_object:jcibot_object")]
        public List<JcibotObjectJcibotObject> objects { get; set; }

        [JsonProperty("jcibot_paper:jcibot_paper")]
        public List<JcibotPaperJcibotPaper> papers { get; set; }
    }

    public class Intent
    {
        public double confidence { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }

    public class JcibotPaperJcibotPaper
    {
        public string body { get; set; }
        public double confidence { get; set; }
        public int end { get; set; }
        public Entities entities { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public int start { get; set; }
        public string type { get; set; }
        public string value { get; set; }
    }

}
