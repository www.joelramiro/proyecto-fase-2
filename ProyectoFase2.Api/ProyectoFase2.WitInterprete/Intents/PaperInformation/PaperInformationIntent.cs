﻿using Microsoft.EntityFrameworkCore;
using ProyectoFase2.Database.Context;
using ProyectoFase2.Database.Models;
using ProyectoFase2.Shared.Models.Manager;
using ProyectoFase2.Shared.Models.Manager.Interpretes;
using ProyectoFase2.WitInterprete.Intents.Paper;
using ProyectoFase2.WitInterprete.Interpretes.WIT.Models.Response;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProyectoFase2.WitInterprete.Intents.PaperInformation
{
    public class PaperInformationIntent : IIntentResult
    {
        private readonly IRepository<PaperResume> paperResumeRepository;
        private readonly IRepository<KeywordPaper> keywordPaperRepository;

        public PaperInformationIntent(
            IRepository<PaperResume> paperResumeRepository,
            IRepository<KeywordPaper> keywordPaperRepository
            )
        {
            this.paperResumeRepository = paperResumeRepository;
            this.keywordPaperRepository = keywordPaperRepository;
        }

        public string IntentName => "get_paper_information";

        public async Task<ServiceResult<InterpreteContentResponse>> Response(string content)
        {
            try
            {
                var myJsonObjectResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<PaperInformationDesiarizable>(content);
                if (myJsonObjectResponse == null)
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "Internal server error." });
                }
                if (myJsonObjectResponse.entities.JcibotPaperNameJcibotPaperName == null || !myJsonObjectResponse.entities.JcibotPaperNameJcibotPaperName.Any())
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se encontraron elementos." });
                }

                var objects = myJsonObjectResponse.entities?.JcibotPaperNameJcibotPaperName?.Select(o => o.Value);
                if (objects == null)
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se pudo interpretar el mensaje." });
                }

                var paperName = objects.FirstOrDefault();

                var PaperResume = await this.paperResumeRepository.All()
                                            .Where(i => 
                                            i.Name.Contains(paperName) ||
                                            InventoryDatabaseContext.SoundEx(i.Name) == InventoryDatabaseContext.SoundEx(paperName)).FirstOrDefaultAsync();


                var send = new List<string>();
                if (PaperResume == null)
                {

                    send = new List<string>() { "Hola. <ul> No se encontró el paper solicitado</ul> " }.ToList();
                }
                else
                {
                    send.Add($"<tr><td>Número</td><td>{PaperResume.Id}</td></tr>");
                    send.Add($"<tr><td>Titulo</td><td>{PaperResume.Name}</td></tr>");
                    send.Add($"<tr><td>Abstract</td><td>{PaperResume.Abstract}</td></tr>");
                    send.Add($"<tr><td>Descripción</td><td>{PaperResume.Description}</td></tr>");
                    send = new List<string>() { "Hola. <table> " }.Union(send).Union(new List<string>() { " </table> " }).ToList();
                }

                var stringJoin = string.Join("", send.ToArray());

                return ServiceResult<InterpreteContentResponse>.SuccessResult(new InterpreteContentResponse
                {
                    HeaderText = string.Empty,
                    Type = InterpreteResponseType.Html,
                    ResultContent = stringJoin,
                });
            }
            catch (Exception e)
            {
                return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "Internal server error." });
            }
        }
    }

    public class KeyValue
    {
        public string Key { get; set; }
        public PaperResume Value { get; set; }
    }
}
