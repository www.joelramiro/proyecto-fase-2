﻿using Microsoft.EntityFrameworkCore;
using ProyectoFase2.Database.Context;
using ProyectoFase2.Database.Models;
using ProyectoFase2.Shared.Models.Manager;
using ProyectoFase2.Shared.Models.Manager.Interpretes;
using ProyectoFase2.WitInterprete.Intents;
using ProyectoFase2.WitInterprete.Interpretes.WIT.Models.Response;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProyectoFase2.WitInterprete.Intents.ItemPosition
{
    public class ItemPositionIntent : IIntentResult
    {
        private readonly IRepository<Item> itemRepository;

        public ItemPositionIntent(IRepository<Item> itemRepository)
        {
            this.itemRepository = itemRepository;
        }

        public string IntentName => "get_item_position";

        public async Task<ServiceResult<InterpreteContentResponse>> Response(string content)
        {


            try
            {
                var myJsonObjectResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemPositionDesiarizable>(content);
                if (myJsonObjectResponse == null)
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "Internal server error." });
                }
                if (myJsonObjectResponse.entities.objets == null || !myJsonObjectResponse.entities.objets.Any())
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se encontraron elementos." });
                }

                var objects = myJsonObjectResponse.entities.objets.Select(o => o.value);
                var list = new List<KeyValue>();
                foreach (var o in objects)
                {
                    var item = await this.itemRepository
                        .All()
                        .Include(u => u.Ubication)
                        .Where(i => InventoryDatabaseContext.SoundEx(i.Name) == InventoryDatabaseContext.SoundEx(o)).FirstOrDefaultAsync();

                    list.Add(new KeyValue { Key = o, Value = item });
                }

                var send = list.Select(i => i.Value == null ? $"<li>{i.Key}: no se encontró.</li>" : $"<li>{i.Key}: se encontró en: {i.Value.Ubication.Description}</li>");

                send = (new List<string>() { "Hola. <ul> " }).Union(send).Union(new List<string>() { " </ul> " });
                var stringJoin = string.Join("", send.ToArray());

                return ServiceResult<InterpreteContentResponse>.SuccessResult(new InterpreteContentResponse 
                {
                    HeaderText = string.Empty,
                    Type = InterpreteResponseType.Html,
                    ResultContent = stringJoin,
                });
            }
            catch (Exception e)
            {
                return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "Internal server error." });
            }
        }
    }

    public class KeyValue
    {
        public string Key { get; set; }
        public Item Value { get; set; }
    }
}
