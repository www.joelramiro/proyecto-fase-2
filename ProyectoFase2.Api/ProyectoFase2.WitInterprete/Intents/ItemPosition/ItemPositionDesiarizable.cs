﻿using Newtonsoft.Json;
using ProyectoFase2.WitInterprete.Interpretes.WIT.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFase2.WitInterprete.Intents.ItemPosition
{
    public class ItemPositionDesiarizable : Root
    {
        
        [JsonProperty("entities")]
        public Entities entities { get; set; }
    }

    public class Entities
    {
        [JsonProperty("jcibot_object:jcibot_object")]
        public List<JcibotObjectJcibotObject> objets { get; set; }

        [JsonProperty("jcibot_question:jcibot_question")]
        public List<JcibotQuestionJcibotQuestion> questions { get; set; }
    }

    public class JcibotObjectJcibotObject
    {
        public string body { get; set; }
        public double confidence { get; set; }
        public int end { get; set; }
        public Entities entities { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public int start { get; set; }
        public string type { get; set; }
        public string value { get; set; }
    }

    public class JcibotQuestionJcibotQuestion
    {
        public string body { get; set; }
        public double confidence { get; set; }
        public int end { get; set; }
        public Entities entities { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public int start { get; set; }
        public string type { get; set; }
        public string value { get; set; }
    }
}
