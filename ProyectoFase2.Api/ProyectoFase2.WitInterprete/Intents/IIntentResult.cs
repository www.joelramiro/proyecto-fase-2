﻿using ProyectoFase2.Shared.Models.Manager;
using ProyectoFase2.Shared.Models.Manager.Interpretes;
using ProyectoFase2.WitInterprete.Interpretes.WIT.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFase2.WitInterprete.Intents
{
    public interface IIntentResult
    {
        public string IntentName { get; }

        public Task<ServiceResult<InterpreteContentResponse>> Response(string content);
    }
}