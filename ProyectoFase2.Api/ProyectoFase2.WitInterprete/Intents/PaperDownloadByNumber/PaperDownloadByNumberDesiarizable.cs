﻿using Newtonsoft.Json;
using ProyectoFase2.WitInterprete.Intents.Paper;
using ProyectoFase2.WitInterprete.Interpretes.WIT.Models.Response;

namespace ProyectoFase2.WitInterprete.Intents.PaperDownloadByNumber
{
    public class PaperDownloadByNumberDesiarizable : Root
    {
        [JsonProperty("entities")]
        public Entities entities { get; set; }
    }

    public class Entities
    {
        [JsonProperty("jcibot_download_paper:jcibot_download_paper")]
        public Jcibot[] jcibot_download_paperjcibot_download_paper { get; set; }

        [JsonProperty("jcibot_number:jcibot_number")]
        public Jcibot[] jcibot_numberjcibot_number { get; set; }

        [JsonProperty("jcibot_paper:jcibot_paper")]
        public Jcibot[] jcibot_paperjcibot_paper { get; set; }
    }

    public partial class Jcibot
    {
        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("confidence")]
        public double Confidence { get; set; }

        [JsonProperty("end")]
        public long End { get; set; }

        [JsonProperty("entities")]
        public Traits Entities { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }

        [JsonProperty("start")]
        public long Start { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
