﻿using Microsoft.EntityFrameworkCore;
using ProyectoFase2.Api.Services.FileService;
using ProyectoFase2.Database.Context;
using ProyectoFase2.Database.Models;
using ProyectoFase2.Shared.Models.Manager;
using ProyectoFase2.Shared.Models.Manager.Interpretes;
using ProyectoFase2.WitInterprete.Intents.Paper;
using ProyectoFase2.WitInterprete.Interpretes.WIT.Models.Response;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProyectoFase2.WitInterprete.Intents.PaperDownloadByNumber
{
    public class PaperDownloadByNumberIntent : IIntentResult
    {
        private readonly IRepository<PaperResume> paperResumeRepository;
        private readonly IRepository<KeywordPaper> keywordPaperRepository;
        private readonly IFileService fileService;

        public PaperDownloadByNumberIntent(
            IRepository<PaperResume> paperResumeRepository,
            IRepository<KeywordPaper> keywordPaperRepository,
            IFileService fileService
            )
        {
            this.paperResumeRepository = paperResumeRepository;
            this.keywordPaperRepository = keywordPaperRepository;
            this.fileService = fileService;
        }

        public string IntentName => "get_paper_file_by_number";

        public async Task<ServiceResult<InterpreteContentResponse>> Response(string content)
        {
            try
            {
                var myJsonObjectResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<PaperDownloadByNumberDesiarizable>(content);
                if (myJsonObjectResponse == null)
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "Internal server error." });
                }
                if (myJsonObjectResponse.entities.jcibot_numberjcibot_number == null || !myJsonObjectResponse.entities.jcibot_numberjcibot_number.Any())
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se encontraron elementos." });
                }

                var objects = myJsonObjectResponse.entities?.jcibot_numberjcibot_number?.Select(o => o.Value);
                if (objects == null)
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se pudo interpretar el mensaje." });
                }

                var paperNumber = Convert.ToInt32(objects.FirstOrDefault());

                var PaperResume = await this.paperResumeRepository.All()
                                            .Where(i => i.Id == paperNumber).FirstOrDefaultAsync();


                var send = new List<string>();
                if (PaperResume == null)
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { " <ul> No se encontró el paper solicitado.</ul>" });
                }

                var file = await this.fileService.GetFile(PaperResume.Uri);

                if (file == null)
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { " <ul> No se encontró el paper solicitado de manera física.</ul>" });

                }

                return ServiceResult<InterpreteContentResponse>.SuccessResult(new InterpreteContentResponse
                {
                    HeaderText = "<ul> El archivo se descargará en el dispositivo</ul> ",
                    Type = InterpreteResponseType.FileDownload,
                    ResultContent = file,
                });

            }
            catch (Exception e)
            {
                return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "Internal server error." });
            }
        }
    }

    public class KeyValue
    {
        public string Key { get; set; }
        public PaperResume Value { get; set; }
    }
}
