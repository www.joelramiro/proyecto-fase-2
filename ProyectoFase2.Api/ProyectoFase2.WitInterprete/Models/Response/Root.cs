﻿using Newtonsoft.Json;

namespace ProyectoFase2.WitInterprete.Interpretes.WIT.Models.Response
{
    public class Root
    {
        [JsonProperty("intents")]
        public List<Intent> Intents { get; set; }
        
        [JsonProperty("text")]
        public string Text { get; set; }
        
        [JsonProperty("traits")]
        public Traits Traits { get; set; }
    }
}
