﻿using Newtonsoft.Json;

namespace ProyectoFase2.WitInterprete.Interpretes.WIT.Models.Response
{
    public class Intent
    {
        [JsonProperty("confidence")]
        public double Confidence { get; set; }
        
        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
