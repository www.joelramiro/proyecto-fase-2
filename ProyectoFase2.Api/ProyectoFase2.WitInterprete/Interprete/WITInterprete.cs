﻿using Microsoft.Extensions.Configuration;
using ProyectoFase2.Shared.Models.Manager;
using ProyectoFase2.Shared.Models.Manager.Interpretes;
using ProyectoFase2.Shared.Models.Manager.Interpretes.HttpClients;
using ProyectoFase2.WitInterprete.Intents;
using ProyectoFase2.WitInterprete.Interpretes.WIT.Models.Response;
using System.Threading;
using System.Web;

namespace ProyectoFase2.WitInterprete.Interpretes.WIT.Interprete
{
    public class WITInterprete : IInterprete
    {
        private readonly IHttpClientWIT httpClientWIT;
        private readonly IConfiguration configuration;
        private readonly IEnumerable<IIntentResult> intents;

        public WITInterprete(
            IHttpClientWIT httpClientWIT, IConfiguration configuration, IEnumerable<IIntentResult> intents)
        {
            this.httpClientWIT = httpClientWIT;
            this.configuration = configuration;
            this.intents = intents;
        }

        public string Key => "WIT";

        public async Task<ServiceResult<InterpreteContentResponse>> InterpreteAsync(string ChannelKey, InterpreteContent interpreteContent)
        {
            try
            {
                var query = HttpUtility.ParseQueryString(string.Empty);
                string version = configuration.GetSection("HttpClients").GetSection("HttpClientWIT")["Version"];

                query["v"] = version;
                query["q"] = interpreteContent.Text;
                string queryString = query.ToString();

                var url = $"message?{queryString}";
                var result = await this.httpClientWIT.HttpClient().GetAsync(url); 

                if (result.IsSuccessStatusCode)
                {
                    var response = await result.Content.ReadAsStringAsync();
                    var myJsonObjectResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<Root>(response);

                    if (myJsonObjectResponse.Intents != null && myJsonObjectResponse.Intents.Any())
                    {
                        var jsonIntentResult = myJsonObjectResponse.Intents
                            .OrderByDescending(d => d.Confidence)
                            .FirstOrDefault();

                        if (jsonIntentResult.Confidence > 0.5)
                        {
                            var intent = this.intents.FirstOrDefault(i => i.IntentName == jsonIntentResult.Name);

                            if (intent != null)
                            {
                                var t = await intent.Response(response);
                                return t;
                            }
                            else
                            {
                                return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se pudo interpretar el mensaje" });
                            }
                        }
                        else
                        {
                            return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se pudo interpretar el mensaje" });

                        }
                    }
                    else
                    {
                        return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se pudo interpretar el mensaje" });

                    }
                }
                else
                {
                    return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se pudo interpretar el mensaje" });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return ServiceResult<InterpreteContentResponse>.ErrorResult(new[] { "No se pudo interpretar el mensaje" });
            }
        }
    }
}