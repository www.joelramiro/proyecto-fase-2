﻿using System;
using System.Threading.Tasks;

namespace ProyectoFase2.Api.Services.FileService
{
    public interface IFileService
    {
        Task<byte[]?> GetPaperFileInBytes(string paperName);
        Task<byte[]?> GetFile(string fileName);
        Task<bool> DeleteFile(string fileName);
        Task<bool> CreateFile(byte[] file, string fileName);
    }
}
