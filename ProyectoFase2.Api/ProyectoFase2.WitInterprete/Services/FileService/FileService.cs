﻿using System;
using System.IO;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace ProyectoFase2.Api.Services.FileService
{
    public class FileService : IFileService
    {
        private string filePath = "File/Papers";

        public FileService()
        {
        }

        public async Task<bool> CreateFile(byte[] file, string fileName)
        {
            try
            {
                if (!Directory.Exists($"{filePath}"))
                {
                    Directory.CreateDirectory($"{filePath}");
                }

                string fullFilePath = Path.Combine(filePath, fileName);

                if (File.Exists(fullFilePath))
                {
                    return false;
                }
                else
                {
                    File.WriteAllBytes(fullFilePath, file);
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<byte[]?> GetPaperFileInBytes(string paperName)
        {
            try
            {
                if (!Directory.Exists($"{filePath}"))
                {
                    Directory.CreateDirectory($"{filePath}");
                }

                string fileExtension = ".pdf";
                string fullFilePath = Path.Combine(filePath, paperName + fileExtension);

                if (File.Exists(fullFilePath))
                {
                    byte[] fileBytes = await File.ReadAllBytesAsync(fullFilePath);
                    return fileBytes;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<byte[]?> GetFile(string fileName)
        {
            try
            {
                if (!Directory.Exists($"{filePath}"))
                {
                    Directory.CreateDirectory($"{filePath}");
                }

                string fullFilePath = Path.Combine(filePath, fileName);

                if (File.Exists(fullFilePath))
                {
                    byte[] fileBytes = await File.ReadAllBytesAsync(fullFilePath);
                    return fileBytes;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public async Task<bool> DeleteFile(string fileName)
        {
            try
            {
                if (!Directory.Exists($"{filePath}"))
                {
                    Directory.CreateDirectory($"{filePath}");
                }

                string fullFilePath = Path.Combine(filePath, fileName);

                if (File.Exists(fullFilePath))
                {
                    File.Delete(fullFilePath);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}
