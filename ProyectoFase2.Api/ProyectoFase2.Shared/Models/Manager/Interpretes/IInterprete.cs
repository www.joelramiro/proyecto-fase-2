﻿// <copyright file="IInterprete.cs" company="JC Company">
// Copyright (c) JC Company. All rights reserved.
// </copyright>

namespace ProyectoFase2.Shared.Models.Manager.Interpretes
{

    public interface IInterprete
    {
        public string Key { get; }

        Task<ServiceResult<InterpreteContentResponse>> InterpreteAsync(string ChannelKey, InterpreteContent interpreteContent);
    }
}