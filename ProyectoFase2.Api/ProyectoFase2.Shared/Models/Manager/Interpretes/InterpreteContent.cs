﻿// <copyright file="InterpreteContent.cs" company="JC Company">
// Copyright (c) JC Company. All rights reserved.
// </copyright>

namespace ProyectoFase2.Shared.Models.Manager.Interpretes
{
    public class InterpreteContentResponse
    {
        public string HeaderText { get; set; }

        public InterpreteResponseType Type { get; set; }

        public object ResultContent { get; set; }
    }

    public class InterpreteContent
    {
        public string Text { get; set; }

    }

    public enum InterpreteResponseType
    {
        Text = 1,
        Html = 2,
        FileDownload = 3,
    }
}
