﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFase2.Shared.Models.Paper
{
    public class PaperEditDTO
    {
        public int Id { get; set; }
        
        public string Title { get; set; }

        public string Description { get; set; }
        
        public string Abstract { get; set; }

        public byte[] File { get; set; }

        public string FileName { get; set; }

        public string Keywords { get; set; }
    }
}
