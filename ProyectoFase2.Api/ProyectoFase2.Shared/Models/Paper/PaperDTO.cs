﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFase2.Shared.Models.Paper
{
    public class PaperDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Abstract { get; set; }

        public string Uri { get; set; }
    }
}
