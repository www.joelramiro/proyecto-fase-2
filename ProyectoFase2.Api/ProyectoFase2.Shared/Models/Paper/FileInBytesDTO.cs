﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFase2.Shared.Models.Paper
{
    public class FileInBytesDTO
    {
        public byte[] File { get; set; }
        public string FileName { get; set; }
    }
}
