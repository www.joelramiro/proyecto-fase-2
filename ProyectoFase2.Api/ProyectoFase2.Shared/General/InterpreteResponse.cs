﻿using ProyectoFase2.Shared.Models.Manager.Interpretes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFase2.Shared.General
{
    public class InterpreteResponse
    {
        public bool IsSuccessed { get; set; }
        public InterpreteContentResponse InterpreteContentResponse { get; set; }

        public IEnumerable<string> Error { get; set; }
    }
}
