﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFase2.Shared.General
{
    public class ListItemDTO<T>
    {
        public IEnumerable<T> Value { get; set; }
    }
}
