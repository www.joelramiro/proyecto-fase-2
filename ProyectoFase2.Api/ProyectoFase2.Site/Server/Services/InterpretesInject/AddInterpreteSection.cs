﻿using ProyectoFase2.Api.Services.FileService;
using ProyectoFase2.Shared.Models.Manager.Interpretes;
using ProyectoFase2.WitInterprete.Intents;
using ProyectoFase2.WitInterprete.Intents.ItemPosition;
using ProyectoFase2.WitInterprete.Intents.Paper;
using ProyectoFase2.WitInterprete.Intents.PaperDownload;
using ProyectoFase2.WitInterprete.Intents.PaperDownloadByNumber;
using ProyectoFase2.WitInterprete.Intents.PaperInformation;
using ProyectoFase2.WitInterprete.Intents.PaperInformationByNumber;
using ProyectoFase2.WitInterprete.Interpretes.WIT.Interprete;

namespace ProyectoFase2.Site.Server.Services.InterpretesInject
{
    public static class AddInterpreteSection
    {
        public static void Add(WebApplicationBuilder builder)
        {
            builder.Services.AddTransient<IInterprete, WITInterprete>();
            builder.Services.AddScoped<IIntentResult, ItemPositionIntent>();
            builder.Services.AddScoped<IIntentResult, PaperResumeFindIntent>();
            builder.Services.AddScoped<IIntentResult, PaperInformationIntent>();
            builder.Services.AddScoped<IIntentResult, PaperDownloadIntent>();
            builder.Services.AddScoped<IIntentResult, PaperDownloadByNumberIntent>();
            builder.Services.AddScoped<IIntentResult, PaperInformationByNumberIntent>();
            builder.Services.AddScoped<IFileService, FileService>();
        }
    }
}
