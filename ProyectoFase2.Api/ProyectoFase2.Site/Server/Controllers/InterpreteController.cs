﻿
using Microsoft.AspNetCore.Mvc;
using ProyectoFase2.Shared.General;
using ProyectoFase2.Shared.Models.Manager.Interpretes;

namespace ProyectoFase2.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InterpreteController : ControllerBase
    {
        private readonly IEnumerable<IInterprete> interpretes;

        public InterpreteController(IEnumerable<IInterprete> interpretes)
        {
            this.interpretes = interpretes;
        }

        [HttpPost()]
        public async Task<IActionResult> Post([FromBody] InterpreteRequest interpreteRequest)
        {
            if (interpreteRequest == null)
            {
                return this.BadRequest();
            }

            var t = this.interpretes.FirstOrDefault(i => i.Key == "WIT");
            var result = await  t.InterpreteAsync(string.Empty, new InterpreteContent
            {
                Text = interpreteRequest.Text,
            });

            if (result.Succeeded)
            {
                return this.Ok(new InterpreteResponse
                {
                    IsSuccessed = true,
                    InterpreteContentResponse = result.Result,
                });
            }
            else
            {
                var m = result.ValidationMessages.Select(m => new { h = m.Key, values = m.Value })
                .SelectMany(sm => sm.values.DefaultIfEmpty(), (h, d) => d);

                return this.Ok(new InterpreteResponse 
                {
                    IsSuccessed = false,
                    Error = m,
                });
            }
        }
    }
}
