﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoFase2.Api.Services.FileService;
using ProyectoFase2.Database.Context;
using ProyectoFase2.Database.Models;
using ProyectoFase2.Shared.General;
using ProyectoFase2.Shared.General.Error;
using ProyectoFase2.Shared.Models.Paper;
using System.Net;

namespace ProyectoFase2.Site.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaperController : ControllerBase
    {
        private readonly IRepository<PaperResume> repository;
        private readonly IFileService fileService;

        public PaperController(IRepository<PaperResume> repository, IFileService fileService)
        {
            this.repository = repository;
            this.fileService = fileService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await this.repository.All()
                    .Select(p => new PaperDTO
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        Abstract = p.Abstract,
                        Uri = p.Uri,
                    }).ToListAsync();

                return Ok(new ListItemDTO<PaperDTO>()
                {
                    Value = result,
                });
            }
            catch (Exception e)
            {
                return this.BadRequest(new Error
                {
                    Code = HttpStatusCode.InternalServerError.ToString(),
                    Target = "Internal",
                    Message = nameof(HttpStatusCode.InternalServerError),
                });
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var result = await this.repository.All()
                    .Include(r => r.KeyWords)
                    .Select(p => new PaperDetailDTO
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        Abstract = p.Abstract,
                        Uri = p.Uri,
                        Keywords = string.Join(",", p.KeyWords.Select(k => k.Name).ToArray()),
                    })
                    .FirstOrDefaultAsync(p => p.Id == id);

                if (result == null)
                {
                    return this.BadRequest(new Error
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Target = nameof(id),
                        Message = "Paper no encontrado.",
                    });
                }

                return Ok(result);
            }
            catch (Exception e)
            {
                return this.BadRequest(new Error
                {
                    Code = HttpStatusCode.InternalServerError.ToString(),
                    Target = "Internal",
                    Message = nameof(HttpStatusCode.InternalServerError),
                });
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] PaperEditDTO paperEditDTO)
        {
            if (paperEditDTO == null)
            {
                return this.BadRequest(new Error
                {
                    Code = HttpStatusCode.NotFound.ToString(),
                    Target = nameof(paperEditDTO),
                    Message = $"El parámetro [{paperEditDTO}] es requerido.",
                });
            }
            try
            {
                var result = await this.repository.All()
                    .Include(r => r.KeyWords)
                    .FirstOrDefaultAsync(p => p.Id == id);

                if (result == null)
                {
                    return this.BadRequest(new Error
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Target = nameof(id),
                        Message = "Paper no encontrado.",
                    });
                }

                result.Name = paperEditDTO.Title;
                result.Description = paperEditDTO.Description;
                result.Abstract = paperEditDTO.Abstract;
                result.KeyWords = paperEditDTO.Keywords.Split(",").Select(k => new KeywordPaper
                {
                    Name = k,
                }).ToList();

                var idUn = Guid.NewGuid().ToString();
                string paperName = $"File_{idUn}_{paperEditDTO.FileName}";
                await fileService.DeleteFile(result.Uri);
                result.Uri = paperName;
                await fileService.CreateFile(paperEditDTO.File, paperName);
                repository.Update(result);
                await repository.SaveChangesAsync();

                return Ok(true);
            }
            catch (Exception e)
            {
                return this.BadRequest(new Error
                {
                    Code = HttpStatusCode.InternalServerError.ToString(),
                    Target = "Internal",
                    Message = nameof(HttpStatusCode.InternalServerError),
                });
            }
        }

        [HttpGet("File/{id}")]
        public async Task<IActionResult> GetPDFByPaperId(int Id)
        {
            try
            {
                var result = await this.repository.All()
                    .FirstOrDefaultAsync(p => p.Id == Id);

                if (result == null)
                {
                    return this.BadRequest(new Error
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Target = nameof(Id),
                        Message = "Paper no encontrado.",
                    });
                }

                var file = await this.fileService.GetFile(result.Uri);

                if (file == null)
                {
                    return this.BadRequest(new Error
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Target = nameof(file),
                        Message = "Documento PDF no encontrado.",
                    });
                }

                return Ok(new FileInBytesDTO
                {
                    File = file,
                    FileName = result.Name,
                });
            }
            catch (Exception e)
            {
                return this.BadRequest(new Error
                {
                    Code = HttpStatusCode.InternalServerError.ToString(),
                    Target = "Internal",
                    Message = nameof(HttpStatusCode.InternalServerError),
                });
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PaperCreateDTO paperCreateDTO)
        {
            try
            {
                if (paperCreateDTO == null)
                {
                    return this.BadRequest(new Error
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Target = nameof(paperCreateDTO),
                        Message = $"El parámetro [{paperCreateDTO}] es requerido.",
                    });
                }

                var exist = await this.repository.FirstOrDefaultAsync(p => p.Name == paperCreateDTO.Title);
                if (exist != null)
                {
                    return this.BadRequest(new Error
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Target = nameof(paperCreateDTO.Title),
                        Message = $"Ya existe un paper con el nombre: [{paperCreateDTO.Title}].",
                    });
                }

                var idUn = Guid.NewGuid().ToString();
                string paperName = $"File_{idUn}_{paperCreateDTO.FileName}";
                this.repository.Create(new PaperResume
                {
                    Abstract = paperCreateDTO.Abstract,
                    Description = paperCreateDTO.Description,
                    Name = paperCreateDTO.Title,
                    Uri = paperName,
                    KeyWords = paperCreateDTO.Keywords.Split(",").Select(k => new KeywordPaper
                    {
                        Name = k,
                    }).ToList(),
                });

                var result = await fileService.CreateFile(paperCreateDTO.File, paperName);

                if (result)
                {
                    await repository.SaveChangesAsync();
                    return this.Ok(true);
                }

                return this.BadRequest(new Error
                {
                    Code = HttpStatusCode.InternalServerError.ToString(),
                    Target = "Internal",
                    Message = "Ocurrió un problema guardando el archivo.",
                });
            }
            catch (Exception e)
            {
                return this.BadRequest(new Error
                {
                    Code = HttpStatusCode.InternalServerError.ToString(),
                    Target = "Internal",
                    Message = nameof(HttpStatusCode.InternalServerError),
                });
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int Id)
        {
            try
            {
                var result = await this.repository.All()
                    .FirstOrDefaultAsync(p => p.Id == Id);

                if (result == null)
                {
                    return this.BadRequest(new Error
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Target = nameof(Id),
                        Message = "Paper no encontrado.",
                    });
                }

                var paperName = result.Uri;
                this.repository.Delete(result);

                var resultFile = await this.fileService.DeleteFile(paperName);

                if (resultFile)
                {
                    await this.repository.SaveChangesAsync();
                    return Ok(true);
                }

                return this.BadRequest(new Error
                {
                    Code = HttpStatusCode.InternalServerError.ToString(),
                    Target = "Internal",
                    Message = "Ocurrió un error eliminando el archivo",
                });

            }
            catch (Exception e)
            {
                return this.BadRequest(new Error
                {
                    Code = HttpStatusCode.InternalServerError.ToString(),
                    Target = "Internal",
                    Message = nameof(HttpStatusCode.InternalServerError),
                });
            }
        }
    }
}
