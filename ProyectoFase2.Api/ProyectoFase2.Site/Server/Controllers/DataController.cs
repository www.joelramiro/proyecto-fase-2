﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoFase2.Database.Context;
using ProyectoFase2.Database.Models;

namespace ProyectoFase2.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataController : ControllerBase
    {
        private readonly InventoryDatabaseContext context;

        public DataController(InventoryDatabaseContext context)
        {
            this.context = context;
        }


        [HttpPost("SeedData")]
        public async Task<IActionResult> FillSeedData()
        {
            if (!context.Ubication.Any())
            {

                // Ubicación 1: Biblioteca
                var biblioteca = new Ubication
                {
                    Name = "Biblioteca",
                    Description = "Ubicación en la biblioteca",
                    Active = true
                };

                biblioteca.Items = new List<Item>
    {
        new Item
        {
            Name = "Libro de Matemáticas",
            Description = "Libro de texto de matemáticas",
            Quantity = 3,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de libros"
            }
        },
        new Item
        {
            Name = "Calculadora científica",
            Description = "Calculadora para cálculos científicos",
            Quantity = 5,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de calculadoras"
            }
        },
        new Item
        {
            Name = "Lápiz",
            Description = "Lápiz para escribir y dibujar",
            Quantity = 10,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de lápices"
            }
        },
        new Item
        {
            Name = "Computadora portátil",
            Description = "Computadora portátil para uso en la biblioteca",
            Quantity = 2,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de computadoras"
            }
        }
    };

                // Ubicación 2: Laboratorio de Química
                var laboratorioQuimica = new Ubication
                {
                    Name = "Laboratorio de Química",
                    Description = "Ubicación en el laboratorio de química",
                    Active = true
                };

                laboratorioQuimica.Items = new List<Item>
    {
        new Item
        {
            Name = "Tubo de ensayo",
            Description = "Recipientes para realizar experimentos",
            Quantity = 50,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de tubos de ensayo"
            }
        },
        new Item
        {
            Name = "Reactivo químico",
            Description = "Sustancia química para realizar reacciones",
            Quantity = 20,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Kilogramo",
                Description = "Unidad de medida de sustancias químicas"
            }
        },
        new Item
        {
            Name = "Microscopio",
            Description = "Instrumento para observar muestras microscópicas",
            Quantity = 3,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de microscopios"
            }
        },
        new Item
        {
            Name = "Guantes de laboratorio",
            Description = "Guantes de protección para trabajar con sustancias peligrosas",
            Quantity = 100,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Par",
                Description = "Unidad de pares de guantes"
            }
        }
    };

                // Ubicación 3: Aula 101
                var aula101 = new Ubication
                {
                    Name = "Aula 101",
                    Description = "Ubicación en el aula 101",
                    Active = true
                };

                aula101.Items = new List<Item>
    {
        new Item
        {
            Name = "Pizarra",
            Description = "Superficie para escribir con tiza o marcadores",
            Quantity = 1,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de pizarras"
            }
        },
        new Item
        {
            Name = "Proyector",
            Description = "Equipo para proyectar imágenes o presentaciones",
            Quantity = 2,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de proyectores"
            }
        },
        new Item
        {
            Name = "Sillas",
            Description = "Asientos para los estudiantes",
            Quantity = 30,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de sillas"
            }
        },
        new Item
        {
            Name = "Mesa del profesor",
            Description = "Mesa para el profesor en el aula",
            Quantity = 1,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de mesas"
            }
        }
    };

                // Ubicación 4: Cafetería
                var cafeteria = new Ubication
                {
                    Name = "Cafetería",
                    Description = "Ubicación en la cafetería",
                    Active = true
                };

                cafeteria.Items = new List<Item>
    {
        new Item
        {
            Name = "Café",
            Description = "Bebida caliente estimulante",
            Quantity = 100,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Taza",
                Description = "Unidad de tazas de café"
            }
        },
        new Item
        {
            Name = "Sándwich de pollo",
            Description = "Sándwich de pollo con lechuga y tomate",
            Quantity = 50,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de sándwiches"
            }
        },
        new Item
        {
            Name = "Refresco",
            Description = "Bebida carbonatada y refrescante",
            Quantity = 200,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Lata",
                Description = "Unidad de latas de refresco"
            }
        },
        new Item
        {
            Name = "Frutas variadas",
            Description = "Variedad de frutas frescas",
            Quantity = 30,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Kilogramo",
                Description = "Unidad de medida de kilogramos"
            }
        }
    };

                // Ubicación 5: Gimnasio
                var gimnasio = new Ubication
                {
                    Name = "Gimnasio",
                    Description = "Ubicación en el gimnasio",
                    Active = true
                };

                gimnasio.Items = new List<Item>
    {
        new Item
        {
            Name = "Cinta de correr",
            Description = "Máquina para correr o caminar",
            Quantity = 5,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de cintas de correr"
            }
        },
        new Item
        {
            Name = "Mancuernas",
            Description = "Pesas para ejercicios de fuerza",
            Quantity = 10,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Par",
                Description = "Unidad de pares de mancuernas"
            }
        },
        new Item
        {
            Name = "Colchonetas",
            Description = "Superficies acolchadas para hacer ejercicios en el suelo",
            Quantity = 20,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de colchonetas"
            }
        },
        new Item
        {
            Name = "Pelotas de ejercicio",
            Description = "Pelotas inflables para ejercicios de equilibrio",
            Quantity = 5,
            MeasurementUnit = new MeasurementUnit
            {
                Name = "Unidad",
                Description = "Unidad de pelotas de ejercicio"
            }
        }
    };

                context.Ubication.AddRange(biblioteca, laboratorioQuimica, aula101, cafeteria, gimnasio);


                context.SaveChanges();

            }

            if (!context.PaperResume.Any())
            {
                var papers = new List<PaperResume>
            {
                new PaperResume
                {
                    Name = "El impacto de la deforestación en la biodiversidad",
                    Description = "Este artículo examina el impacto de la deforestación en la diversidad biológica de los ecosistemas.",
                    Abstract = "La deforestación es una de las principales causas de pérdida de biodiversidad en el mundo...",
                    Uri = "https://ejemplo.com/articulo-1",
                    KeyWords = new List<KeywordPaper>
                    {
                        new KeywordPaper { Name = "Deforestación" },
                        new KeywordPaper { Name = "Biodiversidad" },
                        new KeywordPaper { Name = "Ecosistemas" }
                    }
                },
                new PaperResume
                {
                    Name = "Avances en el tratamiento del cáncer de pulmón",
                    Description = "Este artículo revisa los avances más recientes en el tratamiento del cáncer de pulmón.",
                    Abstract = "El cáncer de pulmón es una enfermedad con altas tasas de mortalidad en todo el mundo...",
                    Uri = "https://ejemplo.com/articulo-2",
                    KeyWords = new List<KeywordPaper>
                    {
                        new KeywordPaper { Name = "Cáncer de pulmón" },
                        new KeywordPaper { Name = "Tratamiento" },
                        new KeywordPaper { Name = "Avances médicos" }
                    }
                },
                new PaperResume
                {
                    Name = "La importancia de las abejas en la polinización de cultivos",
                    Description = "Este artículo explora el papel crucial de las abejas en la polinización de los cultivos y la producción de alimentos.",
                    Abstract = "Las abejas desempeñan un papel fundamental en la polinización de una amplia variedad de cultivos...",
                    Uri = "https://ejemplo.com/articulo-3",
                    KeyWords = new List<KeywordPaper>
                    {
                        new KeywordPaper { Name = "Abejas" },
                        new KeywordPaper { Name = "Polinización" },
                        new KeywordPaper { Name = "Agricultura" }
                    }
                },
                new PaperResume
                {
                    Name = "El impacto del cambio climático en los ecosistemas marinos",
                    Description = "Este artículo analiza los efectos del cambio climático en los ecosistemas marinos y la vida acuática.",
                    Abstract = "El cambio climático ha provocado cambios significativos en los océanos y mares...",
                    Uri = "https://ejemplo.com/articulo-4",
                    KeyWords = new List<KeywordPaper>
                    {
                        new KeywordPaper { Name = "Cambio climático" },
                        new KeywordPaper { Name = "Ecosistemas marinos" },
                        new KeywordPaper { Name = "Biodiversidad marina" }
                    }
                },
                new PaperResume
                {
                    Name = "Nuevas terapias para el tratamiento del Alzheimer",
                    Description = "Este artículo revisa las terapias más recientes y prometedoras para el tratamiento del Alzheimer.",
                    Abstract = "El Alzheimer es una enfermedad neurodegenerativa que afecta a millones de personas en todo el mundo...",
                    Uri = "https://ejemplo.com/articulo-5",
                    KeyWords = new List<KeywordPaper>
                    {
                        new KeywordPaper { Name = "Alzheimer" },
                        new KeywordPaper { Name = "Terapias" },
                        new KeywordPaper { Name = "Investigación médica" }
                    }
                },
                new PaperResume
                {
                    Name = "La importancia de la energía renovable en la lucha contra el cambio climático",
                    Description = "Este artículo examina la relevancia de la energía renovable como alternativa a los combustibles fósiles en la mitigación del cambio climático.",
                    Abstract = "El cambio climático es uno de los desafíos más apremiantes de nuestro tiempo, y la transición...",
                    Uri = "https://ejemplo.com/articulo-6",
                    KeyWords = new List<KeywordPaper>
                    {
                        new KeywordPaper { Name = "Energía renovable" },
                        new KeywordPaper { Name = "Cambio climático" },
                        new KeywordPaper { Name = "Sostenibilidad" }
                    }
                },
                new PaperResume
                {
                    Name = "Avances en la inteligencia artificial aplicada a la medicina",
                    Description = "Este artículo presenta los avances más recientes en la aplicación de la inteligencia artificial en el campo de la medicina.",
                    Abstract = "La inteligencia artificial (IA) ha demostrado un gran potencial en la mejora de los diagnósticos...",
                    Uri = "https://ejemplo.com/articulo-7",
                    KeyWords = new List<KeywordPaper>
                    {
                        new KeywordPaper { Name = "Inteligencia artificial" },
                        new KeywordPaper { Name = "Medicina" },
                        new KeywordPaper { Name = "Diagnóstico" }
                    }
                },
                new PaperResume
                {
                    Name = "El impacto de los microorganismos en la salud humana",
                    Description = "Este artículo explora la relación entre los microorganismos y la salud humana, incluyendo el microbioma humano y las enfermedades asociadas.",
                    Abstract = "Los microorganismos, como bacterias, virus y hongos, juegan un papel fundamental en nuestra salud...",
                    Uri = "https://ejemplo.com/articulo-8",
                    KeyWords = new List<KeywordPaper>
                    {
                        new KeywordPaper { Name = "Microorganismos" },
                        new KeywordPaper { Name = "Microbioma humano" },
                        new KeywordPaper { Name = "Salud" }
                    }
                },
                new PaperResume
                {
                    Name = "Desarrollo de vacunas contra enfermedades emergentes",
                    Description = "Este artículo aborda el desarrollo y la importancia de las vacunas en la prevención de enfermedades emergentes y pandemias.",
                    Abstract = "Las enfermedades emergentes representan una amenaza significativa para la salud pública global...",
                    Uri = "https://ejemplo.com/articulo-9",
                    KeyWords = new List<KeywordPaper>
                    {
                        new KeywordPaper { Name = "Vacunas" },
                        new KeywordPaper { Name = "Enfermedades emergentes" },
                        new KeywordPaper { Name = "Prevención" }
                    }
                },
                new PaperResume
                {
                    Name = "El impacto de la contaminación del aire en la salud respiratoria",
                    Description = "Este artículo examina los efectos de la contaminación del aire en la salud de las vías respiratorias y los pulmones.",
                    Abstract = "La contaminación del aire es un problema ambiental grave que afecta a millones de personas...",
                    Uri = "https://ejemplo.com/articulo-10",
                    KeyWords = new List<KeywordPaper>
                    {
                        new KeywordPaper { Name = "Contaminación del aire" },
                        new KeywordPaper { Name = "Salud respiratoria" },
                        new KeywordPaper { Name = "Enfermedades pulmonares" }
                    }
                }
            };

                context.PaperResume.AddRange(papers);
                context.SaveChanges();
            }

            return this.Ok();
        }
    }
}
