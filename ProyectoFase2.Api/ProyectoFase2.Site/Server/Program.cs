using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using ProyectoFase2.Database.Context;
using ProyectoFase2.Database.Models;
using ProyectoFase2.Database.Repository.InventoryRepository;
using ProyectoFase2.Database.Seed;
using ProyectoFase2.Shared.StaticValues;
using ProyectoFase2.Site.Server.Services.HttpClients;
using ProyectoFase2.Site.Server.Services.InterpretesInject;

namespace ProyectoFase2.Site
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.Configure<KestrelServerOptions>(options =>
            {
                options.Limits.MaxRequestBodySize = int.MaxValue; // O el tama�o m�ximo deseado
            });

            builder.Services.AddControllersWithViews();
            builder.Services.AddRazorPages();
            var defaultConnectionString = builder.Configuration.GetConnectionString(StaticVariables.InventoryDbConnectionName) ?? throw new InvalidOperationException("Connection string 'InventoryDbConnection' not found.");
            builder.Services.AddDbContext<InventoryDatabaseContext>(options =>
                options.UseSqlServer(defaultConnectionString));
            AddInterpreteSection.Add(builder);
            AddHttpClientsSection.Add(builder);

            builder.Services.AddScoped<IRepository<Ubication>, UbicationRepository>();
            builder.Services.AddScoped<IRepository<Item>, ItemRepository>();
            builder.Services.AddScoped<IRepository<PaperResume>, PaperResumeRepository>();
            builder.Services.AddScoped<IRepository<KeywordPaper>, KeywordPaperRepository>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            try
            {
                IServiceScope? scope = builder.Services.BuildServiceProvider().GetRequiredService<IServiceScopeFactory>().CreateScope();
                await InventoryDatabaseInitialDataSeed.SeedAsync(scope.ServiceProvider);

            }
            catch (Exception e)
            {
                var message = e.Message;
            }


            app.UseHttpsRedirection();

            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();


            app.MapRazorPages();
            app.MapControllers();
            app.MapFallbackToFile("index.html");

            app.Run();
        }
    }
}