﻿using Radzen;

namespace ProyectoFase2.Site.Client.Services
{
    public class NotificationCustomerService : INotificationCustomerService
    {
        private readonly NotificationService notificationService;
        private double durationTime = 3000;

        public NotificationCustomerService(NotificationService NotificationService)
        {
            notificationService = NotificationService;
        }

        public void Notify(string message, NotificationSeverity notificationSeverity)
        {
            notificationService.Notify(new NotificationMessage
            {
                Severity = NotificationSeverity.Error,
                Summary = string.Empty,
                Detail = message,
                Duration = durationTime
            });
        }

        public void Notify(string title, string message, NotificationSeverity notificationSeverity)
        {
            notificationService.Notify(new NotificationMessage
            {
                Severity = NotificationSeverity.Error,
                Summary = title,
                Detail = message,
                Duration = durationTime
            });
        }

        public void Notify(string title, string message, NotificationSeverity notificationSeverity, double? duration)
        {
            notificationService.Notify(new NotificationMessage
            {
                Severity = NotificationSeverity.Error,
                Summary = title,
                Detail = message,
                Duration = duration
            });
        }
    }
}
