﻿using Radzen;

namespace ProyectoFase2.Site.Client.Services
{
    public interface INotificationCustomerService
    {
        void Notify(string message ,NotificationSeverity notificationSeverity);
        void Notify(string title, string message ,NotificationSeverity notificationSeverity);
        void Notify(string title, string message ,NotificationSeverity notificationSeverity, double? duration);
    }
}
