﻿using Microsoft.AspNetCore.Components;
using Radzen;

namespace ProyectoFase2.Site.Client.Services
{
    public interface IToolTipCustomer
    {
        void ShowToolTip(ElementReference elementReference, string text, TooltipOptions options = null);
    }
}
