﻿using Radzen;
using static System.Net.WebRequestMethods;
using System.Text;
using System.Text.Json;
using ProyectoFase2.Shared.Models.Manager;
using ProyectoFase2.Shared.General;
using ProyectoFase2.Shared.Models.Paper;
using System.Net.Http.Json;
using ProyectoFase2.Shared.General.Error;

namespace ProyectoFase2.Site.Client.Services
{
    public class HTTPClientRequestService : IHTTPClientRequestService
    {
        private readonly HttpClient http;

        public HTTPClientRequestService(HttpClient Http)
        {
            http = Http;
        }

        public async Task<ServiceResult<T>> Post<T>(string uri, object data)
        {
            try
            {
                string jsonString = JsonSerializer.Serialize(data);
                HttpContent content = new StringContent(jsonString, Encoding.UTF8, "application/json");
                HttpResponseMessage request = await http.PostAsync(uri, content);

                if (request.IsSuccessStatusCode)
                {
                    var response = await request.Content.ReadFromJsonAsync<T>();
                    if (response != null)
                    {
                        return ServiceResult<T>.SuccessResult(response);
                    }
                }
                else
                {
                    var response = await request.Content.ReadFromJsonAsync<Error>();
                    if (response != null)
                    {
                        return ServiceResult<T>.ErrorResult(new[] { response.Message });
                    }
                }

                return ServiceResult<T>.ErrorResult(new[] { "ERROR INTERNO." });
            }
            catch (Exception e)
            {
                return ServiceResult<T>.ErrorResult(new[] { e.Message });
            }
        }

        public async Task<ServiceResult<T>> Put<T>(string uri, object data)
        {
            try
            {
                string jsonString = JsonSerializer.Serialize(data);
                HttpContent content = new StringContent(jsonString, Encoding.UTF8, "application/json");
                HttpResponseMessage request = await http.PutAsync(uri, content);

                if (request.IsSuccessStatusCode)
                {
                    var response = await request.Content.ReadFromJsonAsync<T>();
                    if (response != null)
                    {
                        return ServiceResult<T>.SuccessResult(response);
                    }
                }
                else
                {
                    var response = await request.Content.ReadFromJsonAsync<Error>();
                    if (response != null)
                    {
                        return ServiceResult<T>.ErrorResult(new[] { response.Message });
                    }
                }

                return ServiceResult<T>.ErrorResult(new[] { "ERROR INTERNO." });
            }
            catch (Exception e)
            {
                return ServiceResult<T>.ErrorResult(new[] { e.Message });
            }
        }

        public async Task<ServiceResult<T>> Get<T>(string uri)
        {
            try
            {
                HttpResponseMessage request = await http.GetAsync(uri);

                if (request.IsSuccessStatusCode)
                {
                    var response = await request.Content.ReadFromJsonAsync<T>();
                    if (response != null)
                    {
                        return ServiceResult<T>.SuccessResult(response);
                    }
                }
                else
                {
                    var response = await request.Content.ReadFromJsonAsync<Error>();
                    if (response != null)
                    {
                        return ServiceResult<T>.ErrorResult(new[] { response.Message });
                    }
                }

                return ServiceResult<T>.ErrorResult(new[] { "ERROR INTERNO." });
            }
            catch (Exception e)
            {
                return ServiceResult<T>.ErrorResult(new[] { e.Message });
            }
        }

        public async Task<ServiceResult<T>> Delete<T>(string uri)
        {
            try
            {
                HttpResponseMessage request = await http.DeleteAsync(uri);

                if (request.IsSuccessStatusCode)
                {
                    var response = await request.Content.ReadFromJsonAsync<T>();
                    if (response != null)
                    {
                        return ServiceResult<T>.SuccessResult(response);
                    }
                }
                else
                {
                    var response = await request.Content.ReadFromJsonAsync<Error>();
                    if (response != null)
                    {
                        return ServiceResult<T>.ErrorResult(new[] { response.Message });
                    }
                }

                return ServiceResult<T>.ErrorResult(new[] { "ERROR INTERNO." });
            }
            catch (Exception e)
            {
                return ServiceResult<T>.ErrorResult(new[] { e.Message });
            }
        }
    }
}
