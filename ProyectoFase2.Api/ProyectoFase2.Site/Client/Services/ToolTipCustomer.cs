﻿using Microsoft.AspNetCore.Components;
using Radzen;

namespace ProyectoFase2.Site.Client.Services
{
    public class ToolTipCustomer : IToolTipCustomer
    {
        private readonly TooltipService tooltipService;

        public ToolTipCustomer(TooltipService tooltipService)
        {
            this.tooltipService = tooltipService;
        }

        public void ShowToolTip(ElementReference elementReference, string text, TooltipOptions options = null)
        {
            if (options == null)
            {
                options = new TooltipOptions()
                {
                    Duration = 1000,
                };
            }

            tooltipService.Open(elementReference, text, options);
        }
    }
}
