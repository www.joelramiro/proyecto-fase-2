﻿using ProyectoFase2.Shared.Models.Manager;

namespace ProyectoFase2.Site.Client.Services
{
    public interface IHTTPClientRequestService
    {
        Task<ServiceResult<T>> Post<T>(string uri, object data);
        Task<ServiceResult<T>> Put<T>(string uri, object data);
        Task<ServiceResult<T>> Get<T>(string uri);
        Task<ServiceResult<T>> Delete<T>(string uri);
    }
}
