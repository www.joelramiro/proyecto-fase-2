﻿using Microsoft.AspNetCore.Components.Forms;
using System.ComponentModel.DataAnnotations;

namespace ProyectoFase2.Site.Client.ViewModels.Paper
{
    public class PaperEditViewModel : PaperCreateViewModel
    {
        public int Id { get; set; }
    }
}
