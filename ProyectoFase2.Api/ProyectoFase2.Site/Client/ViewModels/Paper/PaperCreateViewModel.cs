﻿using Microsoft.AspNetCore.Components.Forms;
using System.ComponentModel.DataAnnotations;

namespace ProyectoFase2.Site.Client.ViewModels.Paper
{
    public class PaperCreateViewModel
    {
        [Required(ErrorMessage = "El campo [{0}] es requerido")]
        public string Title { get; set; }


        [Required(ErrorMessage = "El campo [{0}] es requerido")]
        public string Description { get; set; }

        [Required(ErrorMessage = "El campo [{0}] es requerido")]
        public string Abstract { get; set; }

        [Required(ErrorMessage = "El campo [{0}] es requerido")]
        public IBrowserFile BrowserFile;

        [Required(ErrorMessage = "El campo [{0}] es requerido")]
        public byte[] File { get; set; }
        
        [Required(ErrorMessage = "El campo [{0}] es requerido")]
        public string Keywords { get; set; }
    }
}
