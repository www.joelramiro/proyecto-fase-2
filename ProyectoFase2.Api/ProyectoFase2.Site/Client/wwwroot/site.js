function dispatchInputFileClick() {
    const inputFile = document.getElementById('fileInput');
    const clickEvent = new MouseEvent('click', {
        view: window,
        bubbles: true,
        cancelable: true
    });
    inputFile.dispatchEvent(clickEvent);
}

function descargarArchivo(bytesBase64, filename) {
    var link = document.createElement('a');
    link.download = filename;
    link.href = "data:application/octet-stream;base64," + bytesBase64;
    //document.body.appendChild(link); // Needed for Firefox
    link.click();
    //document.body.removeChild(link);
}
function scrollToEnd() {
    var element = document.querySelector('.rz-datalist-data:last-child');
    if (element) {
        element.scrollIntoView();
    }
}
