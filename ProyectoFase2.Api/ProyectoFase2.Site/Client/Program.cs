using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using ProyectoFase2.Site.Client;
using ProyectoFase2.Site.Client.Services;
using Radzen;

namespace ProyectoFase2.Site.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");
            builder.RootComponents.Add<HeadOutlet>("head::after");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            builder.Services.AddScoped<DialogService>();
            builder.Services.AddScoped<NotificationService>();
            builder.Services.AddScoped<TooltipService>();
            builder.Services.AddScoped<ContextMenuService>();
            builder.Services.AddScoped<INotificationCustomerService, NotificationCustomerService>();
            builder.Services.AddScoped<IToolTipCustomer, ToolTipCustomer>();
            builder.Services.AddScoped<IHTTPClientRequestService, HTTPClientRequestService>();
            await builder.Build().RunAsync();
        }
    }
}