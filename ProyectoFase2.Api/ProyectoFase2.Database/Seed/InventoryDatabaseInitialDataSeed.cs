﻿using Microsoft.Extensions.DependencyInjection;
using ProyectoFase2.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFase2.Database.Seed
{
    public static class InventoryDatabaseInitialDataSeed
    {

        public static async Task SeedAsync(IServiceProvider serviceProvider)
        {
            bool databaseCreated = false;
            try
            {
                databaseCreated = await serviceProvider.GetRequiredService<InventoryDatabaseContext>().Database.EnsureCreatedAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

            if (!databaseCreated)
            {
                return;
            }

            var context = serviceProvider.GetRequiredService<InventoryDatabaseContext>();
            await CreateSeed(context);
        }
        private static async Task CreateSeed(InventoryDatabaseContext context)
        {
            try
            {
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
