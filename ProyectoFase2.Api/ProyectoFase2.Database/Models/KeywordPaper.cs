﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoFase2.Database.Models
{
    public class KeywordPaper
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int IdPaperResume { get; set; }


        [ForeignKey(nameof(IdPaperResume))]
        public virtual PaperResume PaperResume{ get; set; }

    }
}