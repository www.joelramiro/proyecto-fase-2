﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.Metrics;

namespace ProyectoFase2.Database.Models
{
    public class Item
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public int IdUbication { get; set; }

        [ForeignKey(nameof(IdUbication))]
        public Ubication Ubication{ get; set; }

        public MeasurementUnit MeasurementUnit { get; set; }

        [ForeignKey(nameof(MeasurementUnit))]
        public int IdMeasurementUnit { get; set; }
    }
}