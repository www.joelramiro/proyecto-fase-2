﻿namespace ProyectoFase2.Database.Models
{
    public class MeasurementUnit
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}