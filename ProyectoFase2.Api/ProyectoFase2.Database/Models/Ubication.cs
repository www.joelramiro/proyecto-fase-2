﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFase2.Database.Models
{
    public class Ubication
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public bool Active { get; set; }

        public ICollection<Item> Items { get; set; }
    }
}
