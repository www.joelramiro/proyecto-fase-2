﻿using ProyectoFase2.Database.Context;
using ProyectoFase2.Database.Models;
using System;
using System.Linq;

namespace ProyectoFase2.Database.Repository.InventoryRepository
{
    public class PaperResumeRepository : InventoryDbContextRepositoryBase<PaperResume>
    {
        public PaperResumeRepository(InventoryDatabaseContext context)
            : base(context)
        {
        }

        public override IQueryable<PaperResume> All()
        {
            return Context.PaperResume;
        }

        protected override PaperResume MapNewValuesToOld(PaperResume oldEntity, PaperResume newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
