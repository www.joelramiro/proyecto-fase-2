﻿using ProyectoFase2.Database.Context;

namespace ProyectoFase2.Database.Repository.InventoryRepository
{
    public abstract class InventoryDbContextRepositoryBase<TEntity> : RepositoryBase<TEntity, InventoryDatabaseContext>
        where TEntity : class
    {
        public InventoryDbContextRepositoryBase(InventoryDatabaseContext context)
            : base(context)
        {
            Context = context;
        }
    }
}

