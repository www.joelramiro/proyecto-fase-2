﻿using ProyectoFase2.Database.Context;
using ProyectoFase2.Database.Models;
using System;
using System.Linq;

namespace ProyectoFase2.Database.Repository.InventoryRepository
{
    public class KeywordPaperRepository : InventoryDbContextRepositoryBase<KeywordPaper>
    {
        public KeywordPaperRepository(InventoryDatabaseContext context)
            : base(context)
        {
        }

        public override IQueryable<KeywordPaper> All()
        {
            return Context.KeywordPaper;
        }

        protected override KeywordPaper MapNewValuesToOld(KeywordPaper oldEntity, KeywordPaper newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
