﻿using ProyectoFase2.Database.Context;
using ProyectoFase2.Database.Models;
using System;
using System.Linq;

namespace ProyectoFase2.Database.Repository.InventoryRepository
{
    public class UbicationRepository : InventoryDbContextRepositoryBase<Ubication>
    {
        public UbicationRepository(InventoryDatabaseContext context)
            : base(context)
        {
        }

        public override IQueryable<Ubication> All()
        {
            return Context.Ubication;
        }

        protected override Ubication MapNewValuesToOld(Ubication oldEntity, Ubication newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
