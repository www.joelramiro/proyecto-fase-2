﻿using ProyectoFase2.Database.Context;
using ProyectoFase2.Database.Models;
using System;
using System.Linq;

namespace ProyectoFase2.Database.Repository.InventoryRepository
{
    public class ItemRepository : InventoryDbContextRepositoryBase<Item>
    {
        public ItemRepository(InventoryDatabaseContext context)
            : base(context)
        {
        }

        public override IQueryable<Item> All()
        {
            return Context.Item;
        }

        protected override Item MapNewValuesToOld(Item oldEntity, Item newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
