﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ProyectoFase2.Database.Models;
using ProyectoFase2.Shared.StaticValues;
using System.Reflection;

namespace ProyectoFase2.Database.Context
{
    public class InventoryDatabaseContext : DbContext
    {
        public InventoryDatabaseContext(DbContextOptions options)
            : base(options)
        {
        }

        protected InventoryDatabaseContext()
        {
        }

        [DbFunction(Name = "SoundEx", IsBuiltIn = true)]
        public static string SoundEx(string input)
        {
            throw new NotImplementedException();
        }

        public DbSet<Ubication> Ubication { get; set; }
        public DbSet<Item> Item { get; set; }
        public DbSet<PaperResume> PaperResume { get; set; }
        public DbSet<KeywordPaper> KeywordPaper { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json")
              .Build();
                var connectionString = configuration.GetConnectionString(StaticVariables.InventoryDbConnectionName);
                optionsBuilder.UseSqlServer(connectionString);
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");
            base.OnModelCreating(modelBuilder);
        }

    }
}
